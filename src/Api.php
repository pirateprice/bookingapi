<?php
declare( strict_types=1 );


namespace ExileeD\Booking;


use GuzzleHttp\ClientInterface;


class Api {

	/**
	 * @var Client
	 */
	private $api;

	/**
	 * Api constructor.
	 *
	 * @param string          $login
	 * @param string          $password
	 * @param ClientInterface $client
	 */
	public function __construct( string $login, string $password, ClientInterface $client = null ) {
		$this->api = new Client( $login, $password, $client );
	}

	/**
	 *  Find a list of all bookable or available rooms at a property
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Availability/blockAvailability
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function blockAvailability( array $params ) {
		return $this->api->request( 'blockAvailability', $params );
	}

	/**
	 *  Find available room for each hotel matching your check-in and check-out dates
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Availability/hotelAvailability
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function hotelAvailability( array $params ) {
		return $this->api->request( 'hotelAvailability', $params );
	}

	/**
	 *  Find available room for each hotel matching your check-in and check-out dates
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/chainTypes
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function chainTypes( array $params ) {
		return $this->api->request( 'chainTypes', $params );
	}


	/**
	 *  Returns all hotel ids which has closed or data has changed since the given timestamp
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/changedHotels
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function changedHotels( array $params ) {
		return $this->api->request( 'changedHotels', $params );
	}

	/**
	 * Returns a list of cities where Booking.com offers hotels.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/cities
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function cities( array $params ) {
		return $this->api->request( 'cities', $params );
	}

	/**
	 * Returns all the where booking.com offers hotels.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/countries
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function countries( array $params ) {
		return $this->api->request( 'countries', $params );
	}

	/**
	 * Returns all the where booking.com offers hotels.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/districts
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function districts( array $params ) {
		return $this->api->request( 'districts', $params );
	}

	/**
	 * Returns facility types names and their translations.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/facilityTypes
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function facilityTypes( array $params ) {
		return $this->api->request( 'facilityTypes', $params );
	}


	/**
	 * Returns hotel facility types names and their translations.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/hotelFacilityTypes
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function hotelFacilityTypes( array $params ) {
		return $this->api->request( 'hotelFacilityTypes', $params );
	}

	/**
	 * Returns the hotel and room data.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/hotels
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function hotels( array $params ) {
		return $this->api->request( 'hotels', $params );
	}

	/**
	 * Returns hotel types names and their translations.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/hotelTypes
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function hotelTypes( array $params ) {
		return $this->api->request( 'hotelTypes', $params );
	}


	/**
	 * Returns a list of payments.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/paymentTypes
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function paymentTypes( array $params ) {
		return $this->api->request( 'paymentTypes', $params );
	}

	/**
	 * Returns all the regions where Booking.com offers hotels.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/regions
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function regions( array $params ) {
		return $this->api->request( 'regions', $params );
	}

	/**
	 * Returns all the regions where Booking.com offers hotels.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/roomFacilityTypes
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function roomFacilityTypes( array $params ) {
		return $this->api->request( 'roomFacilityTypes', $params );
	}


	/**
	 * Returns room types names and their translations.
	 *
	 * @param array $params
	 *
	 * @see https://developers.booking.com/api/technical.html?version=2.0#!/Static/roomTypes
	 * @return mixed
	 * @throws Exception\BookingException
	 */
	public function roomTypes( array $params ) {
		return $this->api->request( 'roomTypes', $params );
	}


}