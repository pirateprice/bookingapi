<?php
declare( strict_types=1 );


namespace ExileeD\Booking;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use ExileeD\Booking\Exception\BookingException;

class Client {

	private const API_URL = 'https://distribution-xml.booking.com/';

	private const API_VERSION = '2.3';

	/**
	 * @var ClientInterface
	 */
	private $client;

	/** @var string */
	private $login;

	/** @var string */
	private $password;

	/**
	 * Client constructor.
	 *
	 * @param string               $login
	 * @param string               $password
	 * @param ClientInterface|null $client
	 */
	public function __construct( string $login, string $password, ClientInterface $client = null ) {

		$this->login    = $login;
		$this->password = $password;
		$this->client   = $client ?? $this->confClient();
	}

	/**
	 * @return GuzzleClient
	 */
	private function confClient():GuzzleClient {
		return new GuzzleClient(
			[ 'base_uri' => $this::API_URL . $this::API_VERSION . '/json/' ]
		);

	}


	/**
	 * @param string $method
	 * @param array  $data
	 *
	 * @return mixed
	 * @throws BookingException
	 */
	public function request( string $method, array $data = [] ) {

		$options = [
			'headers' => [
				'Content-type' => 'application/json',
			],
			'auth'    => [
				$this->login,
				$this->password,
			],
			'query'   => $data,

			'verify' => false, //@todo remove it
		];

		try {
			$response = $this->client->request( 'GET', $method, $options );

			$result = json_decode( $response->getBody()->getContents() );

		} catch ( ClientException | GuzzleException $e ) {
			throw new BookingException( $e->getMessage() );
		}

		if ( isset( $result->errors ) ) {
			throw new BookingException( $result->errors->message );
		}

		return $result->result;
	}
}