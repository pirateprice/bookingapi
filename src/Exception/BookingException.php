<?php
declare( strict_types=1 );


namespace ExileeD\Booking\Exception;


class BookingException extends \Exception {
}